# Adjust number values using scroll (UNMAINTAINED)

Native support for this feature has been introduced in 
[Bug 1261673](https://bugzilla.mozilla.org/show_bug.cgi?id=1261673),
so this extension is no longer maintained.

## a Firefox extension

This extension allows you to adjust numeric values in inputs.
Focus input type='number' and scroll to change its value.

## Installation 

[Adjust number values using scroll on Firefox Addons](https://addons.mozilla.org/de/firefox/addon/adjust-number-values-scroll/)

## License 

This code is licensed under [Mozilla Public License version 2](https://www.mozilla.org/en-US/MPL/2.0/)
