function callback(event){
    var target = event.target;
    var lowerLimit = target.hasAttribute('min') ? target.min : -Infinity;
    var upperLimit = target.hasAttribute('max') ? target.max : Infinity;
    var step = target.hasAttribute('step') ? target.step : 1;
    var factor = event.deltaY < 0 ? 1 : -1;

    var newValue = parseFloat(target.value) + factor * step;
    target.value = Math.min(upperLimit, Math.max(lowerLimit, newValue));

    event.preventDefault();
}

document.addEventListener('focus', function(event) {
    var target = event.target;
    if (target.type == 'number') {
      target.addEventListener('wheel', callback, false);
    }
}, true);
